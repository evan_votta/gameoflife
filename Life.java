import java.util.Scanner;

public class Life {
	int[][] matrix;
	int numTurns;
	int numDeadCells;
	boolean isOver;
	
	public Life(int[][] matrix) {
		this.matrix = matrix;
		numTurns = 0;
		numDeadCells = 0;
		isOver = false;
	}
	
	public void runWholeGame() {
		while (numDeadCells <= (matrix.length * matrix[0].length)) {
			// There are still some live cells
			evaluate();
		}
	}
	
	public void runTurn() {
		if (numDeadCells <= (matrix.length * matrix[0].length)) {
			evaluate();
		}
	}
	
	public void evaluate() {
		int[][] tempMatrix = new int[matrix.length][matrix[0].length];
		
		numDeadCells = 0;
		int cellNum = 0;
		
		for (int row = 0; row < matrix.length; row++) {
			for (int col = 0; col < matrix[row].length; col++) {
				int numLiveNeighbors = 0;
				cellNum++;
				// Check north neighbor
				if (row > 0) {
					if (matrix[row-1][col] == 1) {
						numLiveNeighbors++;
					}
					
					// Check northeast
					if (col < matrix[row].length-1) {
						if (matrix[row-1][col+1] == 1) {
							numLiveNeighbors++;
						}
					}
					
					// Check northwest
					if (col > 0) {
						if (matrix[row-1][col-1] == 1) {
							numLiveNeighbors++;
						}
					}
				}
				
				// Check south neighbor
				if (row < matrix.length-1) {
					if (matrix[row+1][col] == 1) {
						numLiveNeighbors++;
					}
					
					// Check southeast
					if (col < matrix[row].length-1) {
						if (matrix[row+1][col+1] == 1) {
							numLiveNeighbors++;
						}
					}
					
					// Check southwest
					if (col > 0) {
						if (matrix[row+1][col-1] == 1) {
							numLiveNeighbors++;
						}
					}
				}
				
				// Check east neighbor
				if (col < matrix[row].length-1) {
					if (matrix[row][col+1] == 1) {
						numLiveNeighbors++;
					}
				}
				
				// Check west neighbor
				if (col > 0) {
					if (matrix[row][col-1] == 1) {
						numLiveNeighbors++;
					}
				}
				
				
				// Check whether or not to kill this cell
				if (numLiveNeighbors < 2) {
					// Underpopulation, kill
					tempMatrix[row][col] = 0;
					numDeadCells++;
				}
				
				else if (numLiveNeighbors > 3) {
					// Overpopulation, kill
					tempMatrix[row][col] = 0;
					numDeadCells++;
				}
				
				else if (numLiveNeighbors == 3) {
					// Cell is alive and resurrected if necessary
					tempMatrix[row][col] = 1;
				}
				
				else if (numLiveNeighbors == 2 && matrix[row][col] == 1) {
					// Cell stays alive
					tempMatrix[row][col] = 1;
				}
				else {
					// Dead cell remains dead if there are exactly two live neighbors
					tempMatrix[row][col] = 0;
					numDeadCells++;
				}
			}
		}
		
		numTurns++;
		for(int i = 0; i < tempMatrix.length; i++)
			matrix[i] = tempMatrix[i].clone();
	}
	
	public void print() {
		String out = new String("Turn " + (numTurns+1));
		for (int row = 0; row < matrix.length; row++) {
			out += "\n|";
			for (int col = 0; col < matrix[row].length; col++) {
				if (matrix[row][col] == 1) {
					out += " # ";
				} else {
					out += " . ";
				}
			}
			out += "|";
		}
		System.out.println("\n" + out + "\n");
	}
	
	public boolean isOver() {
		if (numDeadCells <= (matrix.length * matrix[0].length)) {
			return false;
		}
		if (isOver) {
			return true;
		}
		return true;
	}
	
	public int getTurns() { return numTurns; }
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		int[][] matrix = {
			{ 0,1,1,1 },
			{ 0,0,1,1 },
			{ 0,0,1,1 },
			{ 0,1,1,1 },
		};
		
		Life life = new Life(matrix);
		life.print();
		
		while (life.isOver() == false) {
			System.out.println("Type 1 to run 1 turn. Type 2 to run out the game. Type 3 to exit prematurely.");
			int resp = scan.nextInt();
			if (resp == 2) { 
				life.runWholeGame();
				life.print();
			} else if (resp == 3) {
				System.out.println("Exited game after " + life.getTurns() + " turns.");
				break;
			}
			else {
				life.runTurn();
				life.print();
			}
		}
	}
}